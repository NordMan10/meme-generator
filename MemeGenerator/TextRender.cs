﻿using System;
using System.Drawing;
using MemeGenerator.Infrastructure.Text;


namespace MemeGenerator
{
    public class TextRender
    {
        public TextRender(Bitmap image, ITextSettings textSettings)
        {
            initImage = image;
            changedImage = new Bitmap(initImage);
            this.textSettings = textSettings;
        }

        private Bitmap initImage;
        private Bitmap changedImage;
        private ITextSettings textSettings;

        public Bitmap GetRenderedImage()
        {
            changedImage = new Bitmap(initImage);
            using (Graphics g = Graphics.FromImage(changedImage))
            {
                Brush textBrush = new SolidBrush(textSettings.TextColor);
                var textSize = GetTextSize(textSettings.Text, textSettings.Font);

                g.DrawString(textSettings.Text, textSettings.Font, textBrush, textSettings.TextLocation.X - (textSize.Width / 2), textSettings.TextLocation.Y);

                g.Save();
            }
            return changedImage;
        }

        private SizeF GetTextSize(string text, Font font)
        {
            var img = new Bitmap(1, 1);
            var drawing = Graphics.FromImage(img);
            return drawing.MeasureString(text, font);
        }
    }
        
}
