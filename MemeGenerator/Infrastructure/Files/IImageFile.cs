﻿using System.IO;

namespace MemeGenerator.Infrastructure.Files
{
    public interface IImageFile
    {
        public FileInfo CurrentPath { get; set; }
        byte[] Get(FileInfo path);
        void Set(FileInfo path, byte[] content);
    }
}