﻿using System.Drawing;

namespace MemeGenerator.Infrastructure.Text
{
    /// <summary>
    /// Содержит сам текст и его настройки для отображения на картинке
    /// </summary>
    public interface ITextSettings
    {
        string Text { get; }
        Color TextColor { get; }
        Font Font { get; }
        Point TextLocation { get; }
    }
}
