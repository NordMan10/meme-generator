﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MemeGenerator.Infrastructure.Text
{
    public class TextSettings : ITextSettings
    {
        public TextSettings(string text, Color textColor, Font font, Point textLocation)
        {
            Text = text;
            TextColor = textColor;
            Font = font;
            TextLocation = textLocation;
        }
        public string Text { get; set; }

        public Color TextColor { get; set; }

        public Font Font { get; set; }

        public Point TextLocation { get; }
    }
}
