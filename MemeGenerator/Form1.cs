﻿using System;
using System.Drawing;
using System.Windows.Forms;
using MemeGenerator.Infrastructure.Text;

namespace MemeGenerator
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
            textSettings = new TextSettings("", Color.White, new Font("Impact", 36), new Point(pictureBox1.Width / 2, 100));
            textRender = new TextRender(new Bitmap("./img/image1.jpg"), textSettings);

            ConfigureMenuStrip();

            ConfigureFontDialog();
        }

        private TextSettings textSettings { get; }
        private TextRender textRender { get; }


        private void ConfigureFontDialog()
        {
            FontChange.Click += FontChange_Click;
            fontDialog1.ShowColor = true;
        }

        private void FontChange_Click(object sender, EventArgs e)
        {
            if (fontDialog1.ShowDialog() == DialogResult.Cancel)
                return;
            // установка шрифта
            textSettings.Font = fontDialog1.Font;
            // установка цвета шрифта
            textSettings.TextColor = fontDialog1.Color;
            Invalidate();
        }

        private void ConfigureMenuStrip()
        {
            var fileItem = new ToolStripMenuItem("Файл");

            var UploadItem = new ToolStripMenuItem("Загрузить");
            UploadItem.Click += UploadItem_Click;
            var SaveItem = new ToolStripMenuItem("Сохранить");
            SaveItem.Click += SaveItem_Click;

            fileItem.DropDownItems.Add(UploadItem);
            fileItem.DropDownItems.Add(SaveItem);

            menuStrip1.Items.Add(fileItem);
        }

        private void SaveItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Сохранить");
        }

        private void UploadItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Загрузить");
        }


        private void RenderText()
        {
            var bmp = textRender.GetRenderedImage();

            pictureBox1.Image = bmp;
        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {
            textSettings.Text = richTextBox1.Text;
            RenderText();
            Invalidate();
        }
    }
}
